<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Joseph Fernandez, B.S. Information Technology">
	<link rel="icon" href="JFavicon.ico">

	<title>Online Portfolio</title>

	<%@ include file="/css/include_css.jsp" %>		

<!-- Carousel styles -->
<style type="text/css">
h2
{
	margin: 0;     
	color: #666;
	padding-top: 90px;
	font-size: 52px;
	font-family: "trebuchet ms", sans-serif;    
}
.item
{
	background: #333;    
	text-align: center;
	height: 300px !important;
}
.carousel
{
  margin: 20px 0px 20px 0px;
}
.bs-example
{
  margin: 20px;
}
</style>
	
</head>
<body>
	
	<%@ include file="/global/nav_global.jsp" %>	
	
	<div class="container">
		 <div class="starter-template">
						<div class="page-header">
						<%@ include file="/global/header.jsp" %>							
						</div>

<!-- Start Bootstrap Carousel  -->
<div class="bs-example">
	<div
      id="myCarousel"
		class="carousel"
		data-interval="6000"
		data-pause="hover"
		data-wrap="true"
		data-keyboard="true"			
		data-ride="carousel">
		
    	<!-- Carousel indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>   
       <!-- Carousel items -->
        <div class="carousel-inner">

				 <div class="active item" style="background: url(img/code.jpg) no-repeat left center; background-size: cover;">
					 <div class="container">
						 <div class="carousel-caption">
								<h3><em>Joseph Fernandez</em></h3>
							 <p class="lead"><em>With this website, I will illustrate my extensive skills developing a web application, utilizing tools such as tomcat, bitbucket, MySQL, and more.</em></p>
					
						 </div>
					 </div>
				 </div>					

            <div class="item" style="background: url(img/serverroom.jpg) no-repeat left center; background-size: cover;">
					<div class="container">
							<div class="carousel-caption">
								   <h3><em>Locally Hosted</em></h3>
								<p class="lead"><em>Anybody can display the easiness of remotely hosting a website, but what about those times where you have no internet connection and want to get some good edits in? This website is hosted locally, and effectively at that.</em></p>
					   
							</div>
						</div>
        
			   </div>
			   
			   <div class="item" style="background: url(img/wutson.jpg) no-repeat left center; background-size: cover;">
				<div class="container">
						<div class="carousel-caption">
							   <h3><em>WELCOME TO LIS4368</em></h3>
						<!--	<p class="lead">Anybody can display the easiness of remotely hosting a website, but what about those times where you have no internet connection and want to get some good edits in? This website is hosted locally, and effectively at that.</p>
						-->
						</div>
					</div>
	
		   </div>

        <!--    <div class="item">
                <h2>Slide 3</h2>
                <div class="carousel-caption">
                  <h3>Third slide label</h3>
                  <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
						 <img src="img/slide3.png" class="img-responsive" alt="Slide 3">									
                </div>
            </div>

        </div> --> 
        <!-- Carousel nav -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
</div>
<!-- End Bootstrap Carousel  -->

 	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
</div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>
	
</body>
</html>

> **NOTE:** This README.md file should be *modified* and placed at the **root of *each* of your repos directories.**

# LIS4368 - Advanced Web Application Development

## Joseph Fernandez

### LIS4368 Requirements

*Course Work Links (Relative link example):*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials

2. [A2 README.md](a2/README.md "My A2 README.md file")

    - Install MySQL
    - Create a New User, inclusive
    - Develop and Deploy a Webapp
    - Deploying Servlet using @WebServlet
    - Links properly display.
    - Screenshot of query results.
    
3. [A3 README.md](a3/README.md "My A3 README.md file")

    - Entity Relationship Diagram (ERD)
    - Include data (at least 10 records each table)
    - Provide Bitbucket read-only access to repo (Language SQL)
        - docs folder: a3.mwb and a3.sql
        - img folder: a3.png (export a3.mwb file as a3.png)
        - README.md (*MUST* display a3.mwb file as a3.png)
    - Bitbucket Repo Push
    
4. [P1 README.md](p1/README.md "My P1 README.md file")

    - Screenshot of Splash Page
    - Screenshot of Failed Validation
    - Screenshot of Passed Validation
    - Ch. 9-10 Questions

5. [A4 README.md](a4/README.md "My A4 README.md file")

    - Screenshot of Failed Validation
    - Screenshot of Passed Validation
    - Ch. 11-12 Questions
    
6. [A5 README.md](a5/README.md "My A5 README.md file")

    - Screenshot of Valid User Form Entry.
    - Screenshot of Passed Validation.
    - Screenshot of Database Entry.
    - Ch. 13-15 Questions.
> **NOTE:** This README.md file should be *modified* and placed at the **root of *each* of your repos directories.**

# LIS4368 - Advanced Web Application Development

## Joseph Fernandez

### Assignment 2 Requirements

>>
[hello](http://localhost:9999/hello)
>>
[HelloHome](http://localhost:9999/hello/HelloHome.html) 
>>
[sayhello](http://localhost:9999/hello/sayhello)
>>
[querybook](http://localhost:9999/hello/querybook.html)
>>
[sayhi](http://localhost:9999/hello/sayhi)
>>
*Screenshot of Query Results*

![querybook Screenshot](img/query.png)
# LIS4368 - Advanced Web Application Development

## Joseph Fernandez

### Project 1 Requirements

*Deliverables*

    1. Screenshot of Splash Page.
    2. Screenshot of Failed Validation.
    3. Screenshot of Passed Validation.
    4. Bitbucket Repo access.
    5. Chapter 9-10 Questions.

#### Project Screenshots

*Screenshot & Link P1 Splash Page*:

![Splash Page](img/homepage.png "Homepage edits based upon P1 Requirements")

*http://localhost:9999/lis4368/index.jsp*

*Screenshot of Failed Validation*:

![Failed Validation](img/fail_vali.png "Failed Validation based upon P1 Requirements")


*Screenshot of Passed Validation*:

![Passed Validation](img/passed_vali.png "Passed Validation based upon P1 Requirements")


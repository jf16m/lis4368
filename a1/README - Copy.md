> **NOTE:** This README.md file should be *modified* and placed at the **root of *each* of your repos directories.**

# LIS4368 - Advanced Web Application Development

## Joseph Fernandez

### Assignment 1 Requirements:

*Course Work Links (Relative link example):*

1. [A1 README.md](a1/README.md "My A1 README.md file")

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of running http://localhost:9999*:

![Tomcat installed and running](img/tomcat.png)

> This is a blockquote.
>
>This is the second paragraph in the blockquote.
>
> This is a blockquote.

> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jf16m/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/jf16m/myteamquotes/ "My Team Quotes Tutorial")
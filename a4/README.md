# LIS4368 - Advanced Web Application Development

## Joseph Fernandez

### Assignment 4 Requirements 

*Deliverables*

    1. Screenshot of Failed Validation
    2. Screenshot of Passed Validation
    3. Ch. 11-12 Questions

#### Assignment Screenshot and Links:

*Screenshot A4 Failed Validation*:

![A4 FAILED VALIDATION](img/failvali.png "Failed Validation based upon A4 Requirements")

*Screenshot A4 Passed Validation*:

![A4 PASSED VALIDATION](img/passvali.png "Passed Validation based upon A4 Requirements") 